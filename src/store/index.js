import Vue from 'vue';
import { createStore } from 'vuex'
import interns from './modules/interns'

export default createStore({
    modules :{
        interns
    }
}

)

