import axios from 'axios';

const state = {
    interns: []
}

const getters = {
    allInterns: (state) => state.interns
};
const actions = {
    async fetchInterns({ commit }) {
        const response = await axios.get('https://jsonplaceholder.typicode.com/users');
        commit('setData', response.data);
    },
    async addIntern({ commit }, name, email) {
        const response = await axios.post(
            'https://jsonplaceholder.typicode.com/users',
            { name, email }
        );
        console.log(response.data);
        commit('newIntern', response.data);
    },
    async deleteIntern({ commit }, id) {
        await axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`);

        commit('removeIntern', id);
    },
    async updateIntern({ commit }, uI) {
        const response = await axios.put(
            `https://jsonplaceholder.typicode.com/users/${uI.id}`,
            uI
        );

        console.log(response.data);

        commit('updateIntern', response.data);
    },
};
const mutations = {
    setData: (state, interns) => (state.interns = interns),
    newIntern: (state, intern) => state.interns.unshift(intern),
    removeIntern: (state, id) =>
        (state.interns = state.interns.filter(intern => intern.id !== id)),
    updateIntern: (state, uI) => {
        const index = state.interns.findIndex(intern => intern.id === uI.id);
        if (index !== -1) {
            state.interns.splice(index, 1, uI);
        }
    }

};

export default {
    state,
    getters,
    actions,
    mutations
};